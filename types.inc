(* define your types here *)

type TWorld = record
    levelNum: byte;
    state: byte;
    name: TString;
    flobx, floby, start: byte;
    startingSlime: word;
    totalSlimeCount: byte;
    secretsCount: byte;
    timeTreshold: word;
    scoreTreshold: word;
    goalcolor1: byte;
    goalcolor2: byte;
    bank: byte;
    assetBank: byte;
    imgBank: byte;
    iconPtr: word;
    rmtPtr: word;
    iconFlobX: byte;
    iconFlobY: byte;
    iconFlobFrame: byte;
    lvlPtr: array [0..32] of word;
end;

type TWorldStats = record
	attempts: word;
	winCount: word;
	failCount: word;
	flawlessCount: word;
	mastersCount: word;
	pickAllCount: word;
	secretAllCount: word;
    speedRunsCount: word;
	maxSlimes: byte;
	maxSecrets: byte;
	minDeaths: word;
	maxDeaths: word;
	bestTime: word;
	totalSlimes: cardinal;
	totalDeaths: cardinal;
	totalTime: cardinal;
	bestScore: word;
	totalScore: cardinal;
end;

type TGlobalStats = record
	attempts: cardinal;
	winCount: cardinal;
	failCount: cardinal;
	totalScore: cardinal;
	totalTime: cardinal;
    totalSlimes: cardinal;
    totalDeaths: cardinal;
    flawlessWorlds: byte;
	allSlimesWorlds: byte;
	allSecretsWorlds: byte;
    mastersWorlds: byte;
    speedRunsWorlds: cardinal;
    flawlessCount: cardinal;
	allSlimesCount: cardinal;
	allSecretsCount: cardinal;
    goalsCount: byte;
end;

type TAchievement = record
	name: string[20];
	desc: string[20];
	value: byte;  
(*    
        0: val := worldStats.attempts;
        1: val := worldStats.winCount;
        2: val := worldStats.failCount;
        3: val := worldStats.totalScore;
        4: val := worldStats.totalSlimes;
        5: val := worldStats.flawlessCount;
        6: val := worldStats.pickAllCount;
        7: val := worldStats.secretAllCount;
        8: val := worldStats.bestScore;
        9: val := worldStats.bestTime;
        10: val := worldStats.totalTime;
        $10: val := globalStats.goalsCount;
        $11: val := globalStats.allSecrets;
        $12: val := globalStats.allSlimes;
        $13: val := globalStats.totalSlimes;
        $14: val := globalStats.winCount;
        $15: val := globalStats.totalScore;
        $16: val := globalStats.flawlessCount;
        $17: val := globalStats.totalDeaths;
        $18: val := achivementsCount;
        $19: val := globalStats.attempts;
        $1A: val := globalStats.totalTime;
*)
	oper: byte; // 0 - greater 
	treshold: cardinal;
end;
	


type TLevel = record
    colors: array [0..3] of byte;
    room_top: byte;
    room_right: byte;
    room_bottom: byte;
    room_left: byte;
end;

type TSprites = record
    state: byte;
    gap: byte;
    PMGwidth: array[0..1] of byte;
    stype: array[0..1] of byte;
    flipable: array[0..1] of byte;
    x: array[0..1] of byte;
    y: array[0..1] of byte;
    goal: array[0..1] of byte;
    frameOn: array[0..1] of byte;
    frameOff: array[0..1] of byte;
    frameSpeed: array[0..1] of byte;
    moves: array[0..1] of byte;
    param1: array[0..1] of byte; // goal
    param2: array[0..1] of byte; // room
end;

type TSpriteData = record
    frameCount: array[0..1] of byte;
    height: array[0..1] of byte;
    bank: byte;
    colors: array[0..1] of word;
    frames: array[0..1, 0..15] of word;
end;

type TAnimationStep = record
    times:byte;
    deltaX:byte;
    deltaY:byte;
    deltaFrame:byte;
end;

type TImage = record
    start: word;
    len: word;
    bank: byte;
end;

type TLastLanded = record
    lvl:byte;
    x:byte;
    y:byte;
    flipped:boolean;
end;

type Drawfloor = (floor_level, roof_level);
