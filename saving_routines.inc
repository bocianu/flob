var 
    lastSave: byte;
    saveAddress: word;
    saveBank: byte;
    worldStats: TWorldStats;
    achievements: array [0..ACHIEVEMENTS_MAX] of byte absolute ACHIEVEMENTS_ADDRESS;
    gameRuns: word absolute GAMERUNS_ADDRESS;
    
    

procedure ResolveSaveAddress(savenum: byte);
begin
    saveBank := SAVE_BASEBANK + (savenum shr 5);
    saveAddress := ($a0 + (savenum and $1F)) shl 8;
    banks[saveBank] := 0;
end;

procedure StoreStats;
begin
    WaitFrame;
    Inc(lastSave,SAVE_PAGES);
    if lastSave = 0 then begin
        EraseSector(SAVE_SECTOR);
    end;
    ResolveSaveAddress(lastSave);
    BurnBlock(saveBank, WORLD_STATS_ADDRESS, saveAddress, SAVE_SIZE);    
    BurnByte(saveBank, saveAddress + ($100 * SAVE_PAGES) - 1, byte('F'));
    SetBank0;
end;

procedure SetWorldStats(worldNum:byte);
var addr:word;
begin
    addr := worldNum shl 6;
    Move(@WorldStats,pointer(WORLD_STATS_ADDRESS+addr),SizeOf(TWorldStats));
end;

procedure ClearWorldStats;
var worldNum:byte;
begin
    FillByte(@WorldStats, SizeOf(TWorldStats), 0);
    WorldStats.minDeaths := $ffff;
    WorldStats.bestTime := $ffff;
    for worldNum := 0 to 5 do SetWorldStats(worldNum);
end;

procedure GetWorldStats(worldNum:byte);
var addr:word;
begin
    addr := worldNum shl 6;
    Move(pointer(WORLD_STATS_ADDRESS+addr), @WorldStats,SizeOf(TWorldStats));
end;

procedure InitGameStats;
begin
    ClearWorldStats;
    FillByte(@achievements, ACHIEVEMENTS_COUNT, 0);
    gameRuns := 0;
    lastSave := 256 - SAVE_PAGES;
end;

procedure RestoreLastSave;
begin
    ResolveSaveAddress(lastSave);
    Move(pointer(saveAddress), pointer(WORLD_STATS_ADDRESS), SAVE_SIZE);
end;

function FindLastSave:boolean;
var save,b:byte;
begin
    result := false;
    save := 0;
    repeat 
        ResolveSaveAddress(save);
        b := peek(saveAddress + $200 - 1);
        if b = byte('F') then begin
            lastSave := save;
            result := true;
        end;
        Inc(save, SAVE_PAGES);
    until (save = 0);
end;

procedure RestoreWorldStats;
begin
    if FindLastSave then RestoreLastSave
        else InitGameStats;
    Inc(gameRuns);
    StoreStats;
end;
