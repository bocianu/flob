for IMAGE in $(ls -1 images/*.gif)
do
    echo \*\*\* CONVERTING $IMAGE
    NAME="${IMAGE%.*}"
    convert $IMAGE -depth 2 -colorspace gray $NAME.gray
    python swapcolors2bpp.py $NAME.gray $NAME.gray1 1 3
    python swapcolors2bpp.py $NAME.gray1 $NAME.gfx 1 2
#    ./rle.exe $NAME.gfx $NAME.rle 
    ./makeLZ4.sh $NAME.gfx $NAME.lz4
#    ./apultra $NAME.gfx $NAME.apu
    mv images/*.gfx . 
    mv images/room_*.lz4 . 
#    mv images/*.apu .
    rm room_*.gfx
    rm $NAME.gray* 

done

dd if=_flob_ingame.rmt of=tmp.rmt bs=1 skip=6
./apultra tmp.rmt worldmusic.apu
rm tmp.rmt
