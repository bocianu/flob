for WORLD in $(ls -1 *.asm)
do
    echo \*\*\* CONVERTING $WORLD
    e:/atari/mads/mads $WORLD 
    NAME="${WORLD%.*}"
    dd bs=1 skip=6 if=$NAME.obx of=$NAME.bin
    rm $NAME.obx
done

