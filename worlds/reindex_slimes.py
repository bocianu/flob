import re
import os
import sys

if len(sys.argv) < 2:
    print('ERROR!!! no input file provided');
    exit(2);

infile = sys.argv[1]

if not os.path.isfile(infile):
    print('ERROR!!! file do not exist:', infile)
    exit(3);
    
bakfile = infile + '.bak'
lines = []
slimes = 0
totalidx = 0;

with open(infile, 'r') as in_file:
    lines = in_file.readlines()

for idx, line in enumerate(lines):
    if line.find('total slime count')!=-1:
        totalline = line;
        totalidx = idx;
        
    if line.find('PUT_SLIME')!=-1:
        elems = line.split(',');
        if len(elems)>3:
            inelem = elems[3];
            outelem = re.sub('[\$]{0,1}[0-9a-f]{1,4}', str(slimes), inelem)
            elems[3] = outelem;
            slimes += 1
            
        lines[idx] = ','.join(elems);

if slimes > 0:
    print('***', slimes, 'slimes reindexed');
    if totalidx != 0:
        nline = re.sub('[0-9]{1,3}', str(slimes), totalline)
        lines[totalidx] = nline
        print('*** total count updated ->', nline.strip());

    os.replace(infile, bakfile)   
        
    with open(infile, 'w') as out_file:
        out_file.writelines(lines)
        print('*** file "' + infile + '" updated');

