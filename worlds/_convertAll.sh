#!/bin/bash
for IMAGE in $(ls -1 ${1}images/*.gif)
do
    echo \*\*\* CONVERTING $IMAGE
    NAME="${IMAGE%.*}"
    magick convert $IMAGE -brightness-contrast 20x0 -depth 2 $NAME.gray
    python swapcolors2bpp.py $NAME.gray $NAME.gray1 1 3
    python swapcolors2bpp.py $NAME.gray1 $NAME.gfx 1 2
#    ./rle.exe $NAME.gfx $NAME.rle 
    if [[ $NAME == *room_* ]]; then
      ./_makeLZ4.sh $NAME.gfx $NAME.lz4
    fi
#    ./apultra $NAME.gfx $NAME.apu
    rm $NAME.gray* 

done

#    mv ${1}images/*.apu $1
    mv ${1}images/*.gfx $1 
    mv ${1}images/room_*.lz4 $1 
    rm ${1}room_*.gfx

dd if=${1}_flob_ingame.rmt of=tmp.rmt bs=1 skip=6
./apultra tmp.rmt ${1}worldmusic.apu
rm tmp.rmt
