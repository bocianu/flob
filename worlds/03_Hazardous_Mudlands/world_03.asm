    OPT f+ h-
    icl '../../const.inc'
    org $a000
world_start ; -- mudlands --  
    
    dta 23 ; number of levels             > | < 
    dta WORLD_PLAYABLE ; state
    dta 18,"Hazardous Mudlands              "  ; name (keep length untouched)
    dta 16,40 ; starting x, starting y, starting room
    dta 13; 13 ; starting room (13)
    dta a($0800) ; starting slime amount
    dta 150 ; total slime count
    dta 2 ; secretsCount
    dta a(100); timeTreshold: word;
    dta a(15000) ;scoreTreshold: cardinal;    
    dta $bc,$b8 ; goal colors
    dta 0; world bank (leave 0 here);
    dta 1; assets bank offset;
    dta 2; image bank offset;
    dta a(icon); icon pointer
    dta a(rmt); rmt pointer
    
    dta 77 ; iconFlobX
    dta 34 ; iconFlobY
    dta 0  ; iconFlobFrame
    
    ;;; ROOM POINTERS
    dta a(room_00)
    dta a(room_01)
    dta a(room_02)
    dta a(room_03)
    dta a(room_04)
    dta a(room_05)
    dta a(room_06)
    dta a(room_07)
    dta a(room_08)
    dta a(room_09)
    dta a(room_10)
    dta a(room_11)
    dta a(room_12)
    dta a(room_13)
    dta a(room_14)
    dta a(room_15)
    dta a(room_16)
    dta a(room_17)
    dta a(room_18)
    dta a(room_19)
    dta a(room_20)
    dta a(room_21)
    dta a(room_22)
    
;;; goals

;; 0 - gear / trapdoor
;; 1 - valve / lake
;; 2 - button / secret
;; 3 - level / hatch
;; 4 - bigslime
;; 5 - goal vial
;; 6 - bigslime
;; 7 - 
;; 8 - 
;; 9 - 
;; a -     
    
    
icon
    dta $d6, $1e, $92 ; icon colors
    ins 'icon.gfx'

room_00    
    ;;  c0   c1   c2   c4
    dta $d6, $1e, $92, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 11, 3, 1, 12    ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 0;    
    dta PUT_SLIME, 54, 2, 0  ; x, row, goal unique num
    dta PUT_SLIME, 166, 3, 1  ; x, row, goal unique num
    dta PUT_SLIME, 136, 7, 2  ; x, row, goal unique num
    dta PUT_SLIME, 80, 10, 3  ; x, row, goal unique num
    dta PUT_SLIME, 150, 12, 4  ; x, row, goal unique num
    dta PUT_SLIME, 182, 13, 5  ; x, row, goal unique num


    dta PUT_SPRITES, a(sprite_lever) ;

    dta SPRITE_COMBINED       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    :2 dta 0     ;    PMG width -> SIZEP2 SIZEP3
    :2 dta STYPE_KEY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    :2 dta 1     ;    flipable: array[0..1] of byte;
    :2 dta 70   ;    x: array[0..1] of byte;
    :2 dta 22   ;    y: array[0..1] of byte;
    :2 dta 3     ;    goal
    :2 dta 0     ;    frameOn: array[0..1] of byte;
    :2 dta 6     ;    frameOff: array[0..1] of byte;
    :2 dta 7     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_NONE    ;    moves: array[0..1] of byte;
    :2 dta 7     ;    param1: array[0..1] of byte; // room
    :2 dta SFX_LEVER     ;    param2: array[0..1] of byte;  // sfx

;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target
	dta SET_TRIGGER, 0, 60, 1, 100, 79, ONCE, NONE, SHOW_MSG, 0, 200   
    dta SET_MESSAGE, MSG_INFO, a(msg_hurry);
   
    
    dta LEVEL_END


room_01    
    ;;  c0   c1   c2   c4
    dta $d4, $1d, $92, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 0, ROOM_NONE, 2, ROOM_NONE     ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 1;   
    
    dta PUT_TILE, ON_NO_GOAL, 0, 29, 67, a(tile_trapdoor) ; condition, param, x, y, tile_address 

    
    dta PUT_SPRITES, a(sprite_drop) ;

    dta SPRITE_COMBINED       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    :2 dta 0     ;    PMG width -> SIZEP2 SIZEP3
    :2 dta STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    :2 dta 1     ;    flipable: array[0..1] of byte;
    :2 dta 72   ;    x: array[0..1] of byte;
    :2 dta 20   ;    y: array[0..1] of byte;
    :2 dta $ff     ;    goal
    :2 dta 0     ;    frameOn: array[0..1] of byte;
    :2 dta 0     ;    frameOff: array[0..1] of byte;
    :2 dta 0     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_ONPATH     ;    moves: array[0..1] of byte;
    :2 dta <path_drop3     ;    param1: array[0..1] of byte; // room
    :2 dta >path_drop3     ;    param2: array[0..1] of byte; 

    dta PUT_SLIME, $7c, 2, 6  ; x, row, goal unique num
    dta PUT_SLIME, $b5, 3, 7  ; x, row, goal unique num
    dta PUT_SLIME, $8c, 5, 8  ; x, row, goal unique num
    dta PUT_SLIME, $39, 10, 9  ; x, row, goal unique num
    dta PUT_SLIME, $85, 11, 10  ; x, row, goal unique num
    dta PUT_SLIME, $ab, 15, 11  ; x, row, goal unique num
    dta PUT_SLIME, $55, 16, 12  ; x, row, goal unique num

    dta LEVEL_END
   
room_02    
    ;;  c0   c1   c2   c4
    dta $e4, $18, $52, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 1, ROOM_NONE, ROOM_NONE, 18     ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 2;    
   
    dta PUT_SLIME, $45, 1, 13  ; x, row, goal unique num
    dta PUT_SLIME, $3b, 2, 14  ; x, row, goal unique num
    dta PUT_SLIME, $97, 4, 15  ; x, row, goal unique num
    dta PUT_SLIME, $6d, 6, 16  ; x, row, goal unique num
    dta PUT_SLIME, $99, 7, 17  ; x, row, goal unique num
    dta PUT_SLIME, $54, 8, 18  ; x, row, goal unique num
    dta PUT_SLIME, $6c, 9, 19  ; x, row, goal unique num
    dta PUT_SLIME, $99, 10, 20  ; x, row, goal unique num
    dta PUT_SLIME, $52, 11, 21  ; x, row, goal unique num
    dta PUT_SLIME, $6d, 12, 22  ; x, row, goal unique num
    dta PUT_SLIME, $98, 13, 23  ; x, row, goal unique num
    dta PUT_SLIME, $51, 14, 24  ; x, row, goal unique num
    dta PUT_SLIME, $97, 16, 25  ; x, row, goal unique num
    dta PUT_SLIME, $3b, 17, 26  ; x, row, goal unique num

;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target
    dta SET_TRIGGER, 1, 10, 60, 40, 79, ONCE, NONE, SHOW_MSG, 0, 200   
    dta SET_MESSAGE, MSG_INFO, a(msg_tight);    
    
    dta LEVEL_END
    
room_03    
    ;;  c0   c1   c2   c4
    dta $d6, $1e, $a2, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 10, ROOM_NONE, 4, 0     ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 3;    

    
    dta PUT_SLIME, $7b, 5, 27  ; x, row, goal unique num
    dta PUT_SLIME, $bf, 7, 28  ; x, row, goal unique num
    dta PUT_SLIME, $81, 8, 29  ; x, row, goal unique num
    dta PUT_SLIME, $48, 14, 30  ; x, row, goal unique num
    dta PUT_SLIME, $98, 16, 31  ; x, row, goal unique num
    dta PUT_SLIME, $9d, 19, 32  ; x, row, goal unique num

    
    dta LEVEL_END
   
room_04    
    ;;  c0   c1   c2   c4
    dta $d4, $ed, $a2, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 3, 7, 5, ROOM_NONE     ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 4;    
    
    dta PUT_SLIME, $69, 2, 33  ; x, row, goal unique num
    dta PUT_SLIME, $96, 3, 34  ; x, row, goal unique num
    dta PUT_SLIME, $45, 4, 35  ; x, row, goal unique num
    dta PUT_SLIME, $a8, 8, 36  ; x, row, goal unique num
    dta PUT_SLIME, $74, 10, 37  ; x, row, goal unique num
    dta PUT_SLIME, $c9, 11, 38  ; x, row, goal unique num
    dta PUT_SLIME, $43, 15, 39  ; x, row, goal unique num
    dta PUT_SLIME, $73, 18, 40  ; x, row, goal unique num
    
    
    
    dta LEVEL_END

room_05    
    ;;  c0   c1   c2   c4
    dta $d2, $ee, $12, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 4, 6, ROOM_NONE, ROOM_NONE     ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 5;    
    
    dta PUT_SPRITES, a(sprite_eye) ;

    dta SPRITE_TWO      ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_ENEMY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 90,9   ;    x: array[0..1] of byte;
    dta 5,40   ;    y: array[0..1] of byte;0
    dta $ff,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 0,0     ;    frameOff: array[0..1] of byte;
    dta 7,0     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ONPATH, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta <path_eye,<path_tongue     ;    param1: array[0..1] of byte; // room
    dta >path_eye,>path_tongue     ;    param2: array[0..1] of byte; 
       
    dta PUT_SLIME, $3f, 3, 41  ; x, row, goal unique num
    dta PUT_SLIME, $c5, 4, 42  ; x, row, goal unique num
    dta PUT_SLIME, $93, 5, 43  ; x, row, goal unique num
    dta PUT_SLIME, $71, 10, 44  ; x, row, goal unique num
    dta PUT_SLIME, $c9, 15, 45  ; x, row, goal unique num
    dta PUT_SLIME, $39, 16, 46  ; x, row, goal unique num
    
    dta LEVEL_END

room_06    
    ;;  c0   c1   c2   c4
    dta $d2, $ea, $12, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 7, 9, ROOM_NONE, 5     ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 6;    
    
    dta PUT_SPRITES, a(sprite_drop) ;

    dta SPRITE_COMBINED       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    :2 dta 0     ;    PMG width -> SIZEP2 SIZEP3
    :2 dta STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    :2 dta 1     ;    flipable: array[0..1] of byte;
    :2 dta 38   ;    x: array[0..1] of byte;
    :2 dta 10   ;    y: array[0..1] of byte;
    :2 dta $ff     ;    goal
    :2 dta 0     ;    frameOn: array[0..1] of byte;
    :2 dta 0     ;    frameOff: array[0..1] of byte;
    :2 dta 0     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_ONPATH     ;    moves: array[0..1] of byte;
    :2 dta <path_drop2     ;    param1: array[0..1] of byte; // room
    :2 dta >path_drop2     ;    param2: array[0..1] of byte; 
       
    
    dta PUT_SLIME, $37, 2, 47  ; x, row, goal unique num
    dta PUT_SLIME, $ba, 3, 48  ; x, row, goal unique num
    dta PUT_SLIME, $c2, 6, 49  ; x, row, goal unique num
    dta PUT_SLIME, $b9, 11, 50  ; x, row, goal unique num
    dta PUT_SLIME, $5c, 12, 51  ; x, row, goal unique num
    dta PUT_SLIME, $41, 14, 52  ; x, row, goal unique num
    
;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target
    dta SET_TRIGGER, 2, 20, 1, 50, 79, ONCE, NONE, SHOW_MSG, 0, 200   
    dta SET_MESSAGE, MSG_INFO, a(msg_stinks);
    
    dta LEVEL_END

room_07    
    ;;  c0   c1   c2   c4
    dta $d4, $fc, $22, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 8, 6, 4     ; neighbouring rooms. CW -> top, right, bottom, left
    dta IMAGE_LZ4, 7;    
    
    dta PUT_TILE, ON_NO_GOAL, 3, 25, 3, a(tile_hatch) ; condition, param, x, y, tile_address     
    
    dta PUT_SLIME, $c6, 2, 53  ; x, row, goal unique num
    dta PUT_SLIME, $9a, 6, 54  ; x, row, goal unique num
    dta PUT_SLIME, $33, 9, 55  ; x, row, goal unique num
    dta PUT_SLIME, $9b, 14, 56  ; x, row, goal unique num
    dta PUT_SLIME, $78, 17, 57  ; x, row, goal unique num
    dta PUT_SLIME, $5d, 19, 58  ; x, row, goal unique num    

    dta SET_TRIGGER, 3, 40, 1, 80, 79, ONCE, NONE, SHOW_MSG, 0, 200   
    dta SET_MESSAGE, MSG_INFO, a(msg_awful);
    
    dta LEVEL_END

room_08    
    ;;  c0   c1   c2   c4
    dta $d4, $2c, $32, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, ROOM_NONE, 9, 7
    dta IMAGE_LZ4, 8;    

    dta PUT_SPRITES, a(sprite_gear) ;

    dta SPRITE_COMBINED       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 8       ;    gap
    :2 dta 0     ;    PMG width -> SIZEP2 SIZEP3
    :2 dta STYPE_KEY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    :2 dta 1     ;    flipable: array[0..1] of byte;
    :2 dta 24   ;    x: array[0..1] of byte;
    :2 dta 3   ;    y: array[0..1] of byte;
    :2 dta 0     ;    goal
    :2 dta 0     ;    frameOn: array[0..1] of byte;
    :2 dta 5     ;    frameOff: array[0..1] of byte;
    :2 dta 7     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_NONE     ;    moves: array[0..1] of byte;
    :2 dta 1     ;    param1: array[0..1] of byte; // room
    :2 dta SFX_GEAR     ;    param2: array[0..1] of byte;  // sfx

    dta PUT_SLIME, $34, 15, 59  ; x, row, goal unique num    

    dta LEVEL_END

room_09    
    ;;  c0   c1   c2   c4
    dta $c2, $ea, $12, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta 8, ROOM_NONE, ROOM_NONE, 6
    dta IMAGE_LZ4, 9;    
    dta PUT_SLIME, $33, 1, 60  ; x, row, goal unique num    


    dta PUT_SPRITES, a(sprite_button) ;

    dta SPRITE_COMBINED       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    :2 dta 0     ;    PMG width -> SIZEP2 SIZEP3
    :2 dta STYPE_KEY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    :2 dta 1     ;    flipable: array[0..1] of byte;
    :2 dta 24   ;    x: array[0..1] of byte;
    :2 dta 5   ;    y: array[0..1] of byte;
    :2 dta 2     ;    goal
    :2 dta 0     ;    frameOn: array[0..1] of byte;
    :2 dta 6     ;    frameOff: array[0..1] of byte;
    :2 dta 7     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_NONE     ;    moves: array[0..1] of byte;
    :2 dta 10     ;    param1: array[0..1] of byte; // room
    :2 dta SFX_SIREN     ;    param2: array[0..1] of byte; // SFX


    dta LEVEL_END

room_10    
    ;;  c0   c1   c2   c4
    dta $e6, $de, $92, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 16, 3, 11
    dta IMAGE_LZ4, 10;    

    dta PUT_TILE, ON_NO_GOAL, 2, 38, 10, a(tile_block10) ; condition, param, x, y, tile_address 

    dta PUT_SPRITES, a(sprite_spiderjump) ;

    dta SPRITE_COMBINED      ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 7       ;    gap
    :2 dta 0     ;    PMG width -> SIZEP2 SIZEP3
    :2 dta STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    :2 dta 1     ;    flipable: array[0..1] of byte;
    :2 dta 48   ;    x: array[0..1] of byte;
    :2 dta 67   ;    y: array[0..1] of byte;
    :2 dta $ff     ;    goal
    :2 dta 1     ;    frameOn: array[0..1] of byte;
    :2 dta 0     ;    frameOff: array[0..1] of byte;
    :2 dta 7     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_ONPATH     ;    moves: array[0..1] of byte;
    :2 dta <path_spiderjump   ;    param1: array[0..1] of byte; // room
    :2 dta >path_spiderjump   ;    param2: array[0..1] of byte;     


    dta PUT_SLIME, $61, 1, 61  ; x, row, goal unique num    
    dta PUT_SLIME, $c3, 2, 62  ; x, row, goal unique num    
    dta PUT_SLIME, $8e, 3, 63  ; x, row, goal unique num    
    dta PUT_SLIME, $ba, 5, 64  ; x, row, goal unique num    
    dta PUT_SLIME, $6b, 7, 65  ; x, row, goal unique num    
    dta PUT_SLIME, $ab, 13, 66  ; x, row, goal unique num    
    dta PUT_SLIME, $84, 14, 67  ; x, row, goal unique num    
    dta PUT_SLIME, $41, 17, 68  ; x, row, goal unique num    

    dta LEVEL_END

room_11    
    ;;  c0   c1   c2   c4
    dta $b6, $1e, $92, $00 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 10, 0, ROOM_NONE
    dta IMAGE_LZ4, 11;    
    
    dta PUT_SLIME, $78, 2, 69  ; x, row, goal unique num    
    dta PUT_SLIME, $3d, 3, 70  ; x, row, goal unique num    
    dta PUT_SLIME, $92, 6, 71  ; x, row, goal unique num    
    dta PUT_SLIME, $60, 10, 72  ; x, row, goal unique num    
    dta PUT_SLIME, $3e, 14, 73  ; x, row, goal unique num    
    dta PUT_SLIME, $b1, 16, 74  ; x, row, goal unique num    
    dta PUT_SLIME, $85, 17, 75  ; x, row, goal unique num    
    dta PUT_SLIME, $5d, 18, 76  ; x, row, goal unique num    
    
    dta LEVEL_END

room_12    
    ;;  c0   c1   c2   c4
    dta $d4, $88, $0,  $72 ; level colors
    ;;  gnd  kill bck  brdr
    dta 14, 0, 19, 13
    dta IMAGE_LZ4, 12;    
    
    dta PUT_SLIME, $35, 2, 77  ; x, row, goal unique num    
    dta PUT_SLIME, $c8, 13, 78  ; x, row, goal unique num    
    dta PUT_SLIME, $4b, 14, 79  ; x, row, goal unique num    
    dta PUT_SLIME, $99, 16, 80  ; x, row, goal unique num    
    
    dta PUT_TILE, ON_NO_GOAL, 1, 12, 71, a(tile_lake) ; condition, param, x, y, tile_address 
    

    dta PUT_SPRITES, a(sprite_eyes) ;

    dta SPRITE_SINGLE       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta 2,0     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,0     ;    flipable: array[0..1] of byte;
    dta 11,0    ;    x: array[0..1] of byte;
    dta 40,0    ;    y: array[0..1] of byte;
    dta $ff,$ff ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 4,0     ;    frameOff: array[0..1] of byte;
    dta 7,0     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ANIMATE,0     ;    moves: array[0..1] of byte;
    dta 0,0     ;    param1: array[0..1] of byte; // goal
    dta 0,0     ;    param2: array[0..1] of byte; // room

;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target
	dta SET_TRIGGER, 4, 60, 1, 100, 79, ONCE, NONE, SHOW_MSG, 0, 200   
    dta SET_MESSAGE, MSG_INFO, a(msg_lake);

    dta LEVEL_END

room_13    
    ;;  c0   c1   c2   c4
    dta $d4, $e2, $0, $62 ; level colors
    ;;  gnd  kill bck  brdr
    dta 15, 12, ROOM_NONE, ROOM_ENTRANCE
    dta IMAGE_LZ4, 13;    

    dta PUT_SPRITES, a(sprite_exit_eyes) ;

    dta SPRITE_TWO       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_EXIT, STYPE_DECOR     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 3,132     ;    x: array[0..1] of byte;
    dta 64,59   ;    y: array[0..1] of byte;
    dta $ff,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 3,0     ;    frameOff: array[0..1] of byte;
    dta 3,3     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_ANIMATE, MOVE_ONPATH    ;    moves: array[0..1] of byte;
    dta 0,<path_cateyes     ;    param1: array[0..1] of byte; // room
    dta 0,>path_cateyes     ;    param2: array[0..1] of byte; 

;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target
	dta SET_TRIGGER, 5, 100, 1, 150, 79, ONCE, NONE, SHOW_MSG, 0, 200   
    dta SET_MESSAGE, MSG_INFO, a(msg_cat);

    dta PUT_SLIME, $c3, 13, 81  ; x, row, goal unique num    
    dta PUT_SLIME, $98, 14, 82  ; x, row, goal unique num    
    dta PUT_SLIME, $7b, 15, 83  ; x, row, goal unique num    
    dta PUT_SLIME, $4c, 16, 84  ; x, row, goal unique num    

    dta LEVEL_END

room_14    
    ;;  c0   c1   c2   c4
    dta $14, $0a, $72, $74 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, ROOM_NONE, 12, ROOM_NONE
    dta IMAGE_LZ4, 14;    

    dta PUT_SLIME, $a0, 1, 85  ; x, row, goal unique num    
    dta PUT_SLIME, $54, 4, 86  ; x, row, goal unique num    
    dta PUT_SLIME, $9d, 7, 87  ; x, row, goal unique num    
    dta PUT_SLIME, $c5, 18, 88  ; x, row, goal unique num    

    dta PUT_SPRITES, a(sprite_bat) ;

    dta SPRITE_COMBINED       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 8       ;    gap
    :2 dta 0     ;    PMG width -> SIZEP2 SIZEP3
    :2 dta STYPE_ENEMY    ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    :2 dta 0     ;    flipable: array[0..1] of byte;
    :2 dta 10   ;    x: array[0..1] of byte;
    :2 dta 40   ;    y: array[0..1] of byte;
    :2 dta $ff     ;    goal
    :2 dta 0     ;    frameOn: array[0..1] of byte;
    :2 dta 0     ;    frameOff: array[0..1] of byte;
    :2 dta 1     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_ONPATH     ;    moves: array[0..1] of byte;
    :2 dta <path_bat1     ;    param1: array[0..1] of byte; // goal
    :2 dta >path_bat1     ;    param2: array[0..1] of byte; // room


    dta LEVEL_END

room_15    
    ;;  c0   c1   c2   c4
    dta $e4, $0a, $62, $64 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, ROOM_NONE, 13, ROOM_NONE
    dta IMAGE_LZ4, 15;    

    dta PUT_SLIME, $47, 5, 89  ; x, row, goal unique num    
    dta PUT_SLIME, $9d, 7, 90  ; x, row, goal unique num    
    dta PUT_SLIME, $4b, 8, 91  ; x, row, goal unique num    
    dta PUT_SLIME, $60, 13, 92  ; x, row, goal unique num    
    dta PUT_SLIME, $c2, 15, 93  ; x, row, goal unique num    
    
    dta PUT_SPRITES, a(sprite_hole) ;
    
    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_PORTAL, STYPE_DECOR     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1    ;    flipable: array[0..1] of byte;
    dta 1,12   ;    x: array[0..1] of byte;
    dta 14,14   ;    y: array[0..1] of byte;
    dta 21,$ff   ;    goal
    dta 145,0     ;    frameOn: array[0..1] of byte; // dial start frame
    dta 20,0     ;    frameOff: array[0..1] of byte; // dial last frame
    dta 0,7     ;    frameSpeed: array[0..1] of byte; // dial goal frame
    dta MOVE_NONE, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta 0,<path_owl  ;    param1: array[0..1] of byte; // addr, goal to set if frame = frameoFF
    dta 0,>path_owl  ;    param2: array[0..1] of byte; // addr, goal to check if active         


;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target
    dta SET_TRIGGER, 6, 0, 0, 38, 28, ON_NO_GOAL, SECRETS_OFFSET + 1, SHOW_MSG, 0, 150 
	dta SET_MESSAGE, MSG_INFO, a(msg_owl_says);      
    
    dta LEVEL_END


room_16    
    ;;  c0   c1   c2   c4
    dta $e6, $3a, $b2, $2 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, ROOM_NONE, 17, 10
    dta IMAGE_LZ4, 16;    

    dta PUT_SPRITES, a(sprite_bat) ;

    dta SPRITE_COMBINED      ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 8       ;    gap
    :2 dta 0     ;    PMG width -> SIZEP2 SIZEP3
    :2 dta STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    :2 dta 0     ;    flipable: array[0..1] of byte;
    :2 dta 10   ;    x: array[0..1] of byte;
    :2 dta 40   ;    y: array[0..1] of byte;
    :2 dta $ff     ;    goal
    :2 dta 0     ;    frameOn: array[0..1] of byte;
    :2 dta 0     ;    frameOff: array[0..1] of byte;
    :2 dta 1     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_ONPATH     ;    moves: array[0..1] of byte;
    :2 dta <path_bat1     ;    param1: array[0..1] of byte; // goal
    :2 dta >path_bat1     ;    param2: array[0..1] of byte; // room


    dta PUT_SLIME, $a5, 2, 94  ; x, row, goal unique num    
    dta PUT_SLIME, $c3, 3, 95  ; x, row, goal unique num    
    dta PUT_SLIME, $a1, 6, 96  ; x, row, goal unique num    
    dta PUT_SLIME, $5e, 7, 97  ; x, row, goal unique num    
    dta PUT_SLIME, $c1, 8, 98  ; x, row, goal unique num    
    dta PUT_SLIME, $38, 9, 99  ; x, row, goal unique num    
    dta PUT_SLIME, $c4, 14, 100  ; x, row, goal unique num    
    dta PUT_SLIME, $6e, 15, 101 ; x, row, goal unique num    
    dta PUT_SLIME, $89, 16, 102 ; x, row, goal unique num    
    dta PUT_SLIME, $38, 17, 103 ; x, row, goal unique num    
    dta PUT_SLIME, $af, 18, 104 ; x, row, goal unique num    

;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target
    dta SET_TRIGGER, 7, 0, 6, 15, 16, ONCE, NONE, REACH_GOAL, SECRETS_OFFSET + 2, 0   
    dta SET_TRIGGER, 8, 0, 6, 15, 16, ONCE, NONE, SHOW_MSG, 0, 250 
	dta SET_MESSAGE, MSG_WARNING, a(msg_secret2);      
        
    dta LEVEL_END

room_17    
    ;;  c0   c1   c2   c4
    dta $d6, $1c, $92, $0 ; level colors
    ;;  gnd  kill bck  brdr
    dta 16, ROOM_NONE, ROOM_NONE, ROOM_NONE
    dta IMAGE_LZ4, 17;    

    dta PUT_SLIME, $56, 4, 105 ; x, row, goal unique num    
    dta PUT_SLIME, $58, 9, 106 ; x, row, goal unique num    
    dta PUT_SLIME, $40, 12, 107 ; x, row, goal unique num    
    dta PUT_SLIME, $57, 16, 108 ; x, row, goal unique num    
    
    dta PUT_SPRITES, a(sprite_bigslime) ;

    dta SPRITE_COMBINED       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    :2 dta 0     ;    PMG width -> SIZEP2 SIZEP3
    :2 dta STYPE_SLIME     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ; 3 - slime stash ;
    :2 dta 1     ;    flipable: array[0..1] of byte;
    :2 dta 75   ;    x: array[0..1] of byte;
    :2 dta 44   ;    y: array[0..1] of byte;
    :2 dta 4     ;    goal
    :2 dta 0     ;    frameOn: array[0..1] of byte;
    :2 dta 0     ;    frameOff: array[0..1] of byte;
    :2 dta 3     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_ANIMATE     ;    moves: array[0..1] of byte;
    :2 dta 0      ;    param1: array[0..1] of byte; // room
    :2 dta $40  ;    param2: array[0..1] of byte; //    
    
;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target
    dta SET_TRIGGER, 9, 40, 30, 120, 79, ONCE, NONE, SHOW_MSG, 0, 200   
    dta SET_MESSAGE, MSG_INFO, a(msg_lost);    
        
    dta LEVEL_END


room_18    
    ;;  c0   c1   c2   c4
    dta $76, $aa, $82, $0 ; level colors
    ;;  gnd  kill bck  brdr
    dta 19, 2, ROOM_NONE, ROOM_NONE
    dta IMAGE_LZ4, 18;    

    dta PUT_SLIME, $6b, 2, 109 ; x, row, goal unique num    
    dta PUT_SLIME, $99, 3, 110 ; x, row, goal unique num    
    dta PUT_SLIME, $6e, 4, 111 ; x, row, goal unique num    
    dta PUT_SLIME, $93, 5, 112 ; x, row, goal unique num    
    dta PUT_SLIME, $71, 6, 113 ; x, row, goal unique num    
    dta PUT_SLIME, $8f, 7, 114 ; x, row, goal unique num    
    dta PUT_SLIME, $73, 8, 115 ; x, row, goal unique num    
    dta PUT_SLIME, $8d, 9, 116 ; x, row, goal unique num    
    dta PUT_SLIME, $61, 10, 117 ; x, row, goal unique num    
    dta PUT_SLIME, $a1, 11, 118 ; x, row, goal unique num    

    dta PUT_SPRITES, a(sprite_valve) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_KEY, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1     ;    flipable: array[0..1] of byte;
    dta 9,86   ;    x: array[0..1] of byte;
    dta 9,26   ;    y: array[0..1] of byte;
    dta 1,$ff     ;    goal
    dta 0,0     ;    frameOn: array[0..1] of byte;
    dta 7,0     ;    frameOff: array[0..1] of byte;
    dta 7,0     ;    frameSpeed: array[0..1] of byte;
    dta MOVE_NONE, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta 12, <path_drop      ;    param1: array[0..1] of byte; // room
    dta SFX_VALVE,  >path_drop     ;    param2: array[0..1] of byte; //    
    
    dta LEVEL_END

room_19    
    ;;  c0   c1   c2   c4
    dta $a4, $7a, $92, $0 ; level colors
    ;;  gnd  kill bck  brdr
    dta 12, ROOM_NONE, 18, 20
    dta IMAGE_LZ4, 19;    

    dta PUT_SLIME, $c4, 1, 119 ; x, row, goal unique num    
    dta PUT_SLIME, $c9, 2, 120 ; x, row, goal unique num    
    dta PUT_SLIME, $c3, 3, 121 ; x, row, goal unique num    
    dta PUT_SLIME, $6a, 8, 122 ; x, row, goal unique num    
    dta PUT_SLIME, $4e, 9, 123 ; x, row, goal unique num    
    dta PUT_SLIME, $35, 10, 124 ; x, row, goal unique num    
    dta PUT_SLIME, $7a, 15, 125 ; x, row, goal unique num    
    dta PUT_SLIME, $be, 16, 126 ; x, row, goal unique num    
  
    dta LEVEL_END

room_20    
    ;;  c0   c1   c2   c4
    dta $94, $8a, $02, $0 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, 19, ROOM_NONE, ROOM_NONE
    dta IMAGE_LZ4, 20;    

    dta PUT_SPRITES, a(sprite_vial) ;

    dta SPRITE_COMBINED       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    :2 dta 0     ;    PMG width -> SIZEP2 SIZEP3
    :2 dta STYPE_GOAL  ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ; 3 - slime; 4 - main goal ;
	:2 dta 1     ;    flipable: array[0..1] of byte;
    :2 dta 74   ;    x: array[0..1] of byte;
    :2 dta 43   ;    y: array[0..1] of byte;
    :2 dta 5     ;    goal
    :2 dta 0     ;    frameOn: array[0..1] of byte;
    :2 dta 0     ;    frameOff: array[0..1] of byte;
    :2 dta 7     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_ANIMATE     ;    moves: array[0..1] of byte;
    :2 dta 0     ;    param1: array[0..1] of byte; // room
    :2 dta 0     ;    param2: array[0..1] of byte; //    

    dta PUT_SLIME, $4d, 3, 127 ; x, row, goal unique num    
    dta PUT_SLIME, $ae, 4, 128 ; x, row, goal unique num    
    dta PUT_SLIME, $4b, 7, 129 ; x, row, goal unique num    
    dta PUT_SLIME, $b1, 8, 130 ; x, row, goal unique num    
    dta PUT_SLIME, $4d, 11, 131 ; x, row, goal unique num    
    dta PUT_SLIME, $ae, 12, 132 ; x, row, goal unique num    
    dta PUT_SLIME, $4f, 15, 133 ; x, row, goal unique num    
    dta PUT_SLIME, $ad, 16, 134 ; x, row, goal unique num    
    
;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target
    dta SET_TRIGGER, 10, 130, 10, 155, 70, ONCE, NONE, SHOW_MSG, 0, 200   
    dta SET_MESSAGE, MSG_INFO, a(msg_found);    

;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target
    dta SET_TRIGGER, 11, 140, 10, 155, 70, ON_GOAL, 5, SHOW_MSG, 1, 200   
    dta SET_MESSAGE, MSG_INFO, a(msg_gohome);    

    
    dta LEVEL_END

room_21    
    ;;  c0   c1   c2   c4
    dta $16, $ba, $92, $0 ; level colors
    ;;  gnd  kill bck  brdr
    dta ROOM_NONE, ROOM_NONE, 22, ROOM_NONE
    dta IMAGE_LZ4, 21;    

    dta PUT_SLIME,$5A,$01,135
    dta PUT_SLIME,$AD,$02,136
    dta PUT_SLIME,$34,$03,137
    dta PUT_SLIME,$8A,$04,138
    dta PUT_SLIME,$4E,$05,139
    dta PUT_SLIME,$A2,$09,140
    dta PUT_SLIME,$36,$0B,141
    dta PUT_SLIME,$B0,$0D,142
    dta PUT_SLIME,$66,$10,143
    
    dta PUT_SPRITES, a(sprite_hole_fly) ;

    dta SPRITE_TWO    ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    dta 0,0     ;    PMG width -> SIZEP2 SIZEP3
    dta STYPE_PORTAL, STYPE_ENEMY     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ;
    dta 1,1    ;    flipable: array[0..1] of byte;
    dta 147,61   ;    x: array[0..1] of byte;
    dta 7,34   ;    y: array[0..1] of byte;
    dta 15,$ff   ;    goal
    dta 8,0     ;    frameOn: array[0..1] of byte; // dial start frame
    dta 18,0     ;    frameOff: array[0..1] of byte; // dial last frame
    dta 8,1     ;    frameSpeed: array[0..1] of byte; // dial goal frame
    dta MOVE_ANIMATE, MOVE_ONPATH     ;    moves: array[0..1] of byte;
    dta 0,<path_fly  ;    param1: array[0..1] of byte; // addr, goal to set if frame = frameoFF
    dta 0,>path_fly  ;    param2: array[0..1] of byte; // addr, goal to check if active         

;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target
    dta SET_TRIGGER, 12, 142, 20, 156, 24, ONCE, NONE, REACH_GOAL, SECRETS_OFFSET + 1, 0   
    dta SET_TRIGGER, 13, 142, 20, 156, 24, ONCE, NONE, SHOW_MSG, 0, 250 
	dta SET_MESSAGE, MSG_WARNING, a(msg_secret1);      
    
    dta LEVEL_END

room_22    
    ;;  c0   c1   c2   c4
    dta $a6, $1a, $b2, $0 ; level colors
    ;;  gnd  kill bck  brdr
    dta 21, ROOM_NONE, ROOM_NONE, ROOM_NONE
    dta IMAGE_LZ4, 22;    

    dta PUT_SLIME,$AE,$03,144
    dta PUT_SLIME,$C4,$07,145
    dta PUT_SLIME,$39,$08,146
    dta PUT_SLIME,$34,$0B,147
    dta PUT_SLIME,$C1,$0F,148
    dta PUT_SLIME,$3B,$10,149
    
    dta PUT_SPRITES, a(sprite_bigslime) ;

    dta SPRITE_COMBINED       ;    state: byte; //state 0 none, 1 single, 2 double, 3 combined (ORED COLORS)
    dta 0       ;    gap
    :2 dta 0     ;    PMG width -> SIZEP2 SIZEP3
    :2 dta STYPE_SLIME     ;    etype: array[0..1] of byte;  //type 0 - decor ; 1 - key ; 2 - enemy ; 3 - slime stash ;
    :2 dta 1     ;    flipable: array[0..1] of byte;
    :2 dta 130   ;    x: array[0..1] of byte;
    :2 dta 24    ;    y: array[0..1] of byte;
    :2 dta 6     ;    goal
    :2 dta 0     ;    frameOn: array[0..1] of byte;
    :2 dta 0     ;    frameOff: array[0..1] of byte;
    :2 dta 3     ;    frameSpeed: array[0..1] of byte;
    :2 dta MOVE_ANIMATE     ;    moves: array[0..1] of byte;
    :2 dta 0      ;    param1: array[0..1] of byte; // room
    :2 dta $40  ;    param2: array[0..1] of byte; //        
    
;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target
    dta SET_TRIGGER, 14, 1, 1, 30, 79, ONCE, NONE, SHOW_MSG, 0, 200   
    dta SET_MESSAGE, MSG_INFO, a(msg_crazy);

;   SET_TRIGGER  id, x1, y1, x2, y2, condition, param, action, target
    dta SET_TRIGGER, 15, 130, 10, 155, 30, ONCE, NONE, SHOW_MSG, 1, 200   
    dta SET_MESSAGE, MSG_INFO, a(msg_crazy2);

    dta LEVEL_END

   
rmt
    ins 'worldmusic.apu'
    
    org $A000,$C000




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; TILES ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


tile_block10
    dta 2,9
    dta %00000101, %01010101
    dta %00000101, %01010101
    dta %00000101, %01010101
    dta %00000101, %01010101
    dta %00000101, %01010101
    dta %00000101, %01010101
    dta %00010101, %01010101
    dta %00010101, %01010101
    dta %00010101, %01010101

tile_lake
    dta 14, 9
    ins 'lake.gfx'

tile_hatch
    dta 6,31
    ins 'hatch.gfx'

tile_trapdoor
    dta 9,12
    ins 'trapdoor.gfx'


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; SPRITES


sprite_lever
    icl 'sprite_lever.a65'
    
sprite_eyes
    icl 'eyes.a65'
    
sprite_bat
    icl 'sprite_bat.a65'
    
sprite_valve
    icl 'sprite_valve.a65'
    
sprite_button
    icl 'sprite_button.a65'

sprite_gear
    icl 'sprite_gear.a65'

sprite_bigslime
    icl 'sprite_bigslime.a65'

sprite_vial
    icl 'sprite_vial.a65'

sprite_exit_eyes
    icl 'sprite_exit_eyes.a65'

sprite_drop
    icl 'sprite_drop.a65'

sprite_spiderjump
    icl 'sprite_spiderjump.a65'

sprite_eye
    icl 'sprite_eye.a65'

sprite_hole_fly
    icl 'sprite_hole_fly.a65'

sprite_hole
    icl 'sprite_hole.a65'


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; PATHS


path_bat1
    dta 124 ; times  ; 0 ends path
    dta 1 ; delta x
    dta 0  ; delta y
    dta 1 ; delta frame or $70 + absolute
    
    dta 124 ; times  ; 0 ends path
    dta -1 ; delta x
    dta 0  ; delta y
    dta 1 ; delta frame or $70 + absolute
    
    dta 0
    
path_drop
    dta 1 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta $71 ; delta frame or $70 + absolute

    dta 1 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta $72 ; delta frame or $70 + absolute

    dta 1 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta $73 ; delta frame or $70 + absolute

    dta 40 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 1 ; delta y
    dta 0 ; delta frame or $70 + absolute

	dta $ff; play fx
	dta SFX_WATERDROP;
	dta 0,0

    dta 1 ; times  ; 0 ends path
    dta 0 ; delta x
    dta -40 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta 1 ; times  ; 0 ends path
    dta 40 ; delta x
    dta 9 ; delta y
    dta $71 ; delta frame or $70 + absolute

    dta 1 ; times  ; 0 ends path
    dta 0 ; delta x
    dta -1 ; delta y
    dta $72 ; delta frame or $70 + absolute
    
    dta 1 ; times  ; 0 ends path
    dta 0 ; delta x
    dta -1 ; delta y
    dta $74 ; delta frame or $70 + absolute

    dta 30 ; times  ; 0 ends path
    dta 0 ; delta x
    dta -1 ; delta y
    dta 0 ; delta frame or $70 + absolute

	dta $ff; play fx
	dta SFX_WATERDROP;
	dta 0,0

    dta 1 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 32 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta 1 ; times  ; 0 ends path
    dta -95 ; delta x
    dta 0 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta 1 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta $71 ; delta frame or $70 + absolute

    dta 1 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta $72 ; delta frame or $70 + absolute

    dta 1 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta $73 ; delta frame or $70 + absolute

    dta 30 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 1 ; delta y
    dta 0 ; delta frame or $70 + absolute

	dta $ff; play fx
	dta SFX_WATERDROP;
	dta 0,0

    dta 1 ; times  ; 0 ends path
    dta 0 ; delta x
    dta -30 ; delta y
    dta $70 ; delta frame or $70 + absolute


    dta 1 ; times  ; 0 ends path
    dta 55 ; delta x
    dta -9 ; delta y
    dta $70 ; delta frame or $70 + absolute
    
    dta 0

path_drop2

    dta 1 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta 50 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 0 ; delta frame or $70 + absolute

    dta 6 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 38 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 1 ; delta y
    dta 0 ; delta frame or $70 + absolute

	dta $ff; play fx
	dta SFX_WATERDROP;
	dta 0,0

    dta 5 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 1 ; times  ; 0 ends path
    dta 0 ; delta x
    dta -38 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta 0


path_drop3

    dta 1 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta 50 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 0 ; delta frame or $70 + absolute

    dta 6 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 44 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 1 ; delta y
    dta 0 ; delta frame or $70 + absolute

	dta $ff; play fx
	dta SFX_WATERDROP;
	dta 0,0

    dta 5 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 1 ; times  ; 0 ends path
    dta 0 ; delta x
    dta -44 ; delta y
    dta $7c ; delta frame or $70 + absolute

    dta 20 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 0 ; delta frame or $70 + absolute
    
    dta 0

path_eye

    dta 10, 0, 0,$70
    dta 2, 0, 0, 1
    dta 2, 0, 0, -1

    dta 10, 0, 0,$70
    dta 5, 0, 0,$75
    dta 3, 0, 0,$74
    dta 4, 0, 0,$75
    dta 3, 0, 0,$76

    dta 10, 0, 0,$70
    dta 2, 0, 0, 1
    dta 2, 0, 0, -1

    dta 10, 0, 0,$70
    dta 5, 0, 0,$74
    dta 3, 0, 0,$75
    dta 4, 0, 0,$74
    dta 3, 0, 0,$73

    dta 15, 0, 0,$70
    dta 2, 0, 0, 1
    dta 2, 0, 0, -1

    dta 10, 0, 0,$70
    dta 5, 0, 0,$76
    dta 3, 0, 0,$75
    dta 4, 0, 0,$76
    dta 3, 0, 0,$73


    dta 0
    
path_tongue

    dta 1 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta $ff; play fx
	dta SFX_SCRAPE;
	dta 0,0    

    dta 12 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 150,0,0,0 // wait 50

    dta 1 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta $ff; play fx
	dta SFX_SCRAPE;
	dta 0,0    

    dta 12 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 200,0,0,0 // wait 50

    dta 1 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta $70 ; delta frame or $70 + absolute

    dta $ff; play fx
	dta SFX_SCRAPE;
	dta 0,0    

    dta 12 ; times  ; 0 ends path
    dta 0 ; delta x
    dta 0 ; delta y
    dta 1 ; delta frame or $70 + absolute

    dta 120,0,0,0 // wait 50


    dta 0

path_spiderjump

    dta 4,0,0,1
    dta 2,-1,-1,0
    dta 2,-1,1,0
    dta 2,0,0,1

    dta 4,0,0,1
    dta 2,-1,-1,0
    dta 2,-1,1,0
    dta 2,0,0,1

    dta 4,0,0,1
    dta 2,-1,-1,0
    dta 2,-1,1,0
    dta 2,0,0,1

    dta 4,0,0,1
    dta 2,-1,-1,0
    dta 2,-1,1,0
    dta 2,0,0,1

    dta 4,0,0,1
    dta 2,-1,-1,0
    dta 2,-1,1,0
    dta 2,0,0,1

    dta 4,0,0,1
    dta 2,-1,-1,0
    dta 2,-1,1,0
    dta 2,0,0,1


    dta 4,0,0,1
    dta 2,1,-1,0
    dta 2,1,1,0
    dta 2,0,0,1

    dta 4,0,0,1
    dta 2,1,-1,0
    dta 2,1,1,0
    dta 2,0,0,1

    dta 4,0,0,1
    dta 2,1,-1,0
    dta 2,1,1,0
    dta 2,0,0,1

    dta 4,0,0,1
    dta 2,1,-1,0
    dta 2,1,1,0
    dta 2,0,0,1

    dta 4,0,0,1
    dta 2,1,-1,0
    dta 2,1,1,0
    dta 2,0,0,1

    dta 4,0,0,1
    dta 2,1,-1,0
    dta 2,1,1,0
    dta 2,0,0,1

    dta 0

path_fly
    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0    
    dta 20,1,0,1
    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0    
    dta 10,-1,1,1
    dta $ff; play fx
	dta SFX_INSECT;
	dta 0,0    
    dta 10,-1,-1,1
    dta 0

path_cateyes
    dta 20,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 1,0,0,$71
    dta 15,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 1,0,0,$71
    dta 40,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 1,0,0,$71
    dta 30,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 1,0,0,$71
    dta 10,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 1,0,0,$71
    dta 25,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 1,0,0,$71
    dta 0

path_owl
    dta 10,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 1,0,0,$71
    dta 25,0,0,$70
    dta 1,0,0,$71
    dta 1,0,0,$72
    dta 1,0,0,$71
    dta 0


.array msg_secret1[80] .byte = 0
   [0]  = "    You have found the Secret Area!     "
   [40] = "            MUDDY TREEHOLE              "
.enda

.array msg_secret2[80] .byte = 0
   [0]  = "    You have found the Secret Area!     "
   [40] = "             SWAMPTROOPER               "
.enda

.array msg_owl_says[80] .byte = 0
   [0]  = "  Wise Owl says: There is a treasure    "
   [40] = "           in my treehole.              "
.enda

.array msg_cat[80] .byte = 0
   [0]  = "        Isn't that the same cat?        "
   [40] = "                Again?                  "
.enda

.array msg_lake[80] .byte = 0
   [0]  = "      This place is a bit scary.        "
   [40] = "                                        "
.enda

.array msg_hurry[80] .byte = 0
   [0]  = "    Let's find what we need quickly,    "
   [40] = "         and get out of here.           "
.enda

.array msg_stinks[80] .byte = 0
   [0]  = "        Something stinks here.          "
   [40] = "                                        "
.enda

.array msg_awful[80] .byte = 0
   [0]  = "      I really hate this place.         "
   [40] = "                                        "
.enda

.array msg_crazy[80] .byte = 0
   [0]  = "                Wow!                    "
   [40] = "   I don't think you're going to try.   "
.enda

.array msg_crazy2[80] .byte = 0
   [0]  = "             Holy Slime!                "
   [40] = "  You are an extremely good flober.     "
.enda

.array msg_lost[80] .byte = 0
   [0]  = "    I think someone got lost here.      "
   [40] = "                                        "
.enda

.array msg_tight[80] .byte = 0
   [0]  = "           Damn tight hole!             "
   [40] = "    I hope it's somewhere close...      "
.enda

.array msg_found[80] .byte = 0
   [0]  = "           And there it is!             "
   [40] = "     Take the bottle and off we go.     "
.enda
 
.array msg_gohome[80] .byte = 0
   [0]  = "      Go back to the cat's grove.       "
   [40] = "                                        "
.enda
 
