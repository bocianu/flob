for IMAGE in $(ls -1 ${1}*.gif)
do
    echo \*\*\* CONVERTING $IMAGE
    NAME="${IMAGE%.*}"
    magick convert $IMAGE[0] -brightness-contrast 20x0 -depth 2 $NAME.gray
    python swapcolors2bpp.py $NAME.gray $NAME.gray1 1 3
    python swapcolors2bpp.py $NAME.gray1 $NAME.gfx 1 2
    #./rle.exe $NAME.gfx $NAME.rle 
    #./_makeLZ4.sh $NAME.gfx $NAME.lz4
    ./apultra $NAME.gfx $NAME.apu
done

for IMAGE in $(ls -1 ${1}*.gif_)
do
    echo \*\*\* CONVERTING $IMAGE
    NAME="${IMAGE%.*}"
    convert $IMAGE[0] -depth 1 -colorspace gray $NAME.gray
    mv $NAME.gray $NAME.gfx
    ./apultra $NAME.gfx $NAME.apu
    #./rle.exe $NAME.gfx $NAME.rle 
    #./makeLZ4.sh $NAME.gfx $NAME.lz4
done

rm ${1}*.gray*

