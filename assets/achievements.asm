;// World achievements 
    icl '../const.inc'

; achievement 0                 
;          "                    "
	dta 13,"First Success       "  ; name (keep length untouched)
	dta 17,"Finish this level   "  ; desc (keep length untouched)
	dta 1  ; value - wins
	dta IS_GREATER  ; oper // 0 - greater ; 1 - less
	dta f(0) ;  treshold: cardinal;

; achievement 1                 
;          "                    "
	dta 12,"Slime Hoover        "  ; name (keep length untouched)
	dta 15,"Clear all slimes    "  ; desc (keep length untouched)
	dta 6  ; value - pick all
	dta IS_GREATER  ; oper // 0 - greater ; 1 - less
	dta f(0) ;  treshold: cardinal;	

; achievement 2
;          "                    "
	dta 14,"Young Sherlock      "  ; name (keep length untouched)
	dta 16,"Find all secrets    "  ; desc (keep length untouched)
	dta 7  ; value - pick all
	dta IS_GREATER  ; oper // 0 - greater ; 1 - less
	dta f(0) ;  treshold: cardinal;		

; achievement 3
;          "                    "
	dta 17,"Untouchable Ninja   "  ; name (keep length untouched)
	dta 15,"Win unscratched     "  ; desc (keep length untouched)
	dta 5  ; value - flawless
	dta IS_GREATER  ; oper // 0 - greater ; 1 - less
	dta f(0) ;  treshold: cardinal;		

; achievement 4
;          "                    "
	dta 17,"Undisputed Master   "  ; name (keep length untouched)
	dta 18,"All slime flawless  "  ; desc (keep length untouched)
	dta 11  ; value - mastercount
	dta IS_GREATER  ; oper // 0 - greater ; 1 - less
	dta f(0) ;  treshold: cardinal;

; achievement 5
;          "                    "
	dta 15,"Restless Runner     "  ; name (keep length untouched)
	dta 11,"Time under          "  ; desc (keep length untouched)
	dta 9 ; value - besttime
	dta TRESHOLD_TIME_REACHED  ; oper // 0 - greater ; 1 - less
	dta f(0) ;  treshold: cardinal;

; achievement 6
;          "                    "
	dta 14,"King of Arcade      "  ; name (keep length untouched)
	dta 12,"Score above         "  ; desc (keep length untouched)
	dta 8  ; value - bestscore
	dta TRESHOLD_SCORE_REACHED  ; oper // 0 - greater ; 1 - less
	dta f(0) ;  treshold: cardinal;

; achievement 7
;          "                    "
	dta 15,"Slime Collector     "  ; name (keep length untouched)
	dta 19,"Collect 2000 slimes "  ; desc (keep length untouched)
	dta 4  ; value - totalslime
	dta IS_GREATER  ; oper // 0 - greater ; 1 - less
	dta f(1999) ;  treshold: cardinal;

; achievement 8
;          "                    "
	dta 11,"Hard Winner         "  ; name (keep length untouched)
	dta 12,"Win 25 times        "  ; desc (keep length untouched)
	dta 1  ; value - wins
	dta IS_GREATER  ; oper // 0 - greater ; 1 - less
	dta f(24) ;  treshold: cardinal;

; achievement 9
;          "                    "
	dta 18,"Unyelding Champion  "  ; name (keep length untouched)
	dta 17,"Flawless 10 times   "  ; desc (keep length untouched)
	dta 5  ; value - flawless
	dta IS_GREATER  ; oper // 0 - greater ; 1 - less
	dta f(9) ;  treshold: cardinal;

; achievement 10
;          "                    "
	dta 11,"Slime Baron         "  ; name (keep length untouched)
	dta 16,"Seize 10k slimes    "  ; desc (keep length untouched)
	dta 4  ; value - totalslime
	dta IS_GREATER  ; oper // 0 - greater ; 1 - less
	dta f(9999) ;  treshold: cardinal;

; achievement 11
;          "                    "
	dta 18,"Ubiquitous Strider  "  ; name (keep length untouched)
	dta 18,"Spend 5 hours here  "  ; desc (keep length untouched)
	dta 10  ; value - totaltime
	dta IS_GREATER  ; oper // 0 - greater ; 1 - less
	dta f(5*3600) ;  treshold: cardinal;			
	
;// Global achievements ********************************************************************************

; achievement 0                 
;          "                    "
	dta 13,"Grande Finale       "  ; name (keep length untouched)
	dta 19,"Get all ingredients "  ; desc (keep length untouched)
	dta $10  ; value - goals
	dta IS_GREATER  ; oper // 0 - greater ; 1 - less
	dta f(5) ;  treshold: cardinal;

; achievement 1                 
;          "                    "
	dta 13,"Get Them All!       "  ; name (keep length untouched)
	dta 17,"Pick all slimes     "  ; desc (keep length untouched)
	dta $12  ; value - all slimes
	dta IS_GREATER  ; oper // 0 - greater ; 1 - less
	dta f(5) ;  treshold: cardinal;

; achievement 2
;          "                    "
	dta 17,"No more Mysteries   "  ; name (keep length untouched)
	dta 16,"Find all secrets    "  ; desc (keep length untouched)
	dta $11  ; value - all secrets
	dta IS_GREATER  ; oper // 0 - greater ; 1 - less
	dta f(5) ;  treshold: cardinal;

; achievement 3
;          "                    "
	dta 19,"Bulletproof Messiah "  ; name (keep length untouched)
	dta 17,"Flawless 33 times   "  ; desc (keep length untouched)
	dta $16  ; value - flawless total
	dta IS_GREATER  ; oper // 0 - greater ; 1 - less
	dta f(32) ;  treshold: cardinal;
    

; achievement 4
;          "                    "
	dta 16,"Grandmaster Flob    "  ; name (keep length untouched)
	dta 18,"Master Every World  "  ; desc (keep length untouched)
	dta $1B  ; value - masters
	dta IS_GREATER  ; oper // 0 - greater ; 1 - less
	dta f(5) ;  treshold: cardinal;


; achievement 5
;          "                    "
	dta 15,"Speedy Gonzales     "  ; name (keep length untouched)
	dta 19,"Speedrun all Worlds "  ; desc (keep length untouched)
	dta $1C  ; value - speedruns
	dta IS_GREATER  ; oper // 0 - greater ; 1 - less
	dta f(5) ;  treshold: cardinal;		

; achievement 6
;          "                    "
	dta 13,"Beat My Score       "  ; name (keep length untouched)
	dta 20,"Get 100k score total"  ; desc (keep length untouched)
	dta $15  ; value - totalscore
	dta IS_GREATER  ; oper // 0 - greater ; 1 - less
	dta f(99999) ;  treshold: cardinal;
        	
; achievement 7
;          "                    "
	dta 15,"Hungry for Pink     "  ; name (keep length untouched)
	dta 16,"Pick 100k slimes    "  ; desc (keep length untouched)
	dta $13  ; value - totalslime
	dta IS_GREATER  ; oper // 0 - greater ; 1 - less
	dta f(99999) ;  treshold: cardinal;
	
; achievement 8
;          "                    "
	dta 14,"Big Big Pharma      "  ; name (keep length untouched)
	dta 14,"Win 300 times       "  ; desc (keep length untouched)
	dta $14  ; value - winCount
	dta IS_GREATER  ; oper // 0 - greater ; 1 - less
	dta f(299) ;  treshold: cardinal;
	
; achievement 9
;          "                    "
	dta 15,"Ultimate Answer     "  ; name (keep length untouched)
	dta 19,"Get 42 Achievements "  ; desc (keep length untouched)
	dta $18  ; value - achievement count
	dta IS_GREATER  ; oper // 0 - greater ; 1 - less
	dta f(41) ;  treshold: cardinal;		
	
; achievement 10
;          "                    "
	dta 13,"The Last Hope       "  ; name (keep length untouched)
	dta 13,"Die 666 times       "  ; desc (keep length untouched)
	dta $17  ; value - death total
	dta IS_GREATER  ; oper // 0 - greater ; 1 - less
	dta f(665) ;  treshold: cardinal;	
		
; achievement 11
;          "                    "
	dta 17,"Fearless Wanderer   "  ; name (keep length untouched)
	dta 18,"Play over 50 hours  "  ; desc (keep length untouched)
	dta $1A  ; value - totaltime
	dta IS_GREATER  ; oper // 0 - greater ; 1 - less
	dta f(50*3600) ;  treshold: cardinal;			

	
	
	
