for IMAGE in $(ls -1 $1)
do
    echo \*\*\* CONVERTING $IMAGE
    NAME="${IMAGE%.*}"
    magick convert $IMAGE[0] -depth 1 -colorspace gray $NAME.gray
    mv $NAME.gray $NAME.gfx
    ./rle.exe $NAME.gfx $NAME.rle 
    ./makeLZ4.sh $NAME.gfx $NAME.lz4
    ./apultra $NAME.gfx $NAME.apu
done

