outsize = 256
outblock = [0] * outsize

for i in xrange(0,outsize):
    
    ntsc = i
    if i >= 0x10 :
        ntsc = i + 0x10;
    if i >= 0xf0 : 
        ntsc = i - 0xe0;
    outblock[i] = ntsc


with open('NTSC.map', 'wb') as out_file:
    out_file.write(bytearray(outblock))

