    
flobFrames0
    dta a(f_flob0_left), a(f_flob0_right) 
    dta a(f_flob0_left_), a(f_flob0_right_)
    dta a(f_flob0_left_fall), a(f_flob0_right_fall)
    dta a(f_flob0_left_splat), a(f_flob0_right_splat)
    dta a(f_flob0_left_move), a(f_flob0_right_move)
    dta a(f_flob0_poof1), a(f_flob0_poof1)
    dta a(f_flob0_poof2), a(f_flob0_poof2)
    dta a(f_flob_poof3), a(f_flob_poof3)

flobFrames1
    dta a(f_flob1_left), a(f_flob1_right)
    dta a(f_flob1_left_), a(f_flob1_right_)
    dta a(f_flob1_left_fall), a(f_flob1_right_fall)
    dta a(f_flob1_left_splat), a(f_flob1_right_splat)
    dta a(f_flob1_left_move), a(f_flob1_right_move)
    dta a(f_flob1_poof1), a(f_flob1_poof1)
    dta a(f_flob1_poof2), a(f_flob1_poof2)
    dta a(f_flob_poof4), a(f_flob_poof4)

larrow
    dta %00000001
    dta %00000001
    dta %00000011
    dta %00000011
    dta %00000111
    dta %00000111
    dta %00001111
    dta %00001111
    dta %00011111
    dta %00011111
    dta %00111111
    dta %00111111
    dta %01111111
    dta %01111111
    dta %11111111
    dta %11111111
    dta %01111111
    dta %01111111
    dta %00111111
    dta %00111111
    dta %00011111
    dta %00011111
    dta %00001111
    dta %00001111
    dta %00000111
    dta %00000111
    dta %00000011
    dta %00000011
    dta %00000001
    dta %00000001
    
rarrow
    dta %10000000
    dta %10000000
    dta %11000000
    dta %11000000
    dta %11100000
    dta %11100000
    dta %11110000
    dta %11110000
    dta %11111000
    dta %11111000
    dta %11111100
    dta %11111100
    dta %11111110
    dta %11111110
    dta %11111111
    dta %11111111
    dta %11111110
    dta %11111110
    dta %11111100
    dta %11111100
    dta %11111000
    dta %11111000
    dta %11110000
    dta %11110000
    dta %11100000
    dta %11100000
    dta %11000000
    dta %11000000
    dta %10000000
    dta %10000000

ach_cursor
    dta %10000100
    dta %10000100
    dta %00000000
    dta %00000000
    dta %00000000
    dta %00000000
    dta %00000000
    dta %00000000
    dta %00000000
    dta %00000000
    dta %10000100
    dta %10000100

    dta %01000000
    dta %01000000
    dta %00000100
    dta %00000100
    dta %00000000
    dta %00000000
    dta %00000000
    dta %00000000
    dta %10000000
    dta %10000000
    dta %00001000
    dta %00001000

    dta %00100000
    dta %00100000
    dta %00000000
    dta %00000000
    dta %00000100
    dta %00000100
    dta %10000000
    dta %10000000
    dta %00000000
    dta %00000000
    dta %00010000
    dta %00010000

    dta %00010000
    dta %00010000
    dta %00000000
    dta %00000000
    dta %10000000
    dta %10000000
    dta %00000100
    dta %00000100
    dta %00000000
    dta %00000000
    dta %00100000
    dta %00100000

    dta %00001000
    dta %00001000
    dta %10000000
    dta %10000000
    dta %00000000
    dta %00000000
    dta %00000000
    dta %00000000
    dta %00000100
    dta %00000100
    dta %01000000
    dta %01000000

ach_off
    dta 3,10
    dta $00, $3c, $00, $03, $ff, $c0, $3f, $ff, $fc, $33, $ff, $cc, $33, $ff, $cc, $0f, $ff, $f0, $00, $ff, $00, $00, $3c, $00, $00, $3c, $00, $03, $ff, $c0

ach_on
    dta 3,10
    dta $00, $14, $00, $01, $aa, $40, $29, $aa, $68, $21, $aa, $48, $21, $aa, $48, $09, $aa, $60, $00, $69, $00, $00, $14, $00, $00, $14, $00, $01, $aa, $40

ach_new
    dta 3,10
    dta $03, $d7, $c0, $0d, $aa, $70, $29, $aa, $68, $21, $aa, $48, $2d, $aa, $78, $09, $aa, $60, $30, $69, $0c, $3f, $14, $fc, $0f, $d7, $f0, $01, $aa, $40

    
f_flob0_left
    dta $00,$00,$04,$2a,$00,$00,$42,0
f_flob0_right
    dta $00,$00,$20,$54,$00,$00,$42,0
f_flob0_left_
    dta $00,$00,$04,$2a,$28,$00,$42,0
f_flob0_right_
    dta $00,$00,$20,$54,$14,$00,$42,0
f_flob0_left_fall
    dta $38,$6c,$44,$00,$28,$00,$44,0
f_flob0_right_fall
    dta $1c,$36,$22,$00,$14,$00,$22,0
f_flob0_left_splat
    dta $00,$00,$00,$7e,$83,$c5,$00,0
f_flob0_right_splat
    dta $00,$00,$00,$7e,$c1,$a3,$00,0
f_flob0_left_move
    dta $00,$00,$0c,$02,$51,$01,$81,$7e
f_flob0_right_move
    dta $00,$00,$30,$40,$8a,$80,$81,$7e
f_flob0_poof1
    dta $00,$3c,$42,$66,$42,$42,$66,0
f_flob0_poof2
    dta $3c,$42,$e7,$a5,$db,$a5,$5a,0
f_flob_poof3
    dta $10,$82,$20,$08,$81,$10,$44,0
f_flob_poof4
    dta $22,$08,$81,$10,$04,$41,$08,0

f_flob1_left
    dta $00,$00,$38,$7c,$56,$7e,$3c,0
f_flob1_right
    dta $00,$00,$1c,$3e,$6a,$7e,$3c,0
f_flob1_left_
    dta $00,$00,$38,$7c,$7e,$7e,$3c,0
f_flob1_right_
    dta $00,$00,$1c,$3e,$7e,$7e,$3c,0
f_flob1_left_fall
    dta $00,$10,$38,$7c,$7c,$54,$38,0
f_flob1_right_fall
    dta $00,$08,$1c,$3e,$3e,$2a,$1c,0
f_flob1_left_splat
    dta $00,$00,$00,$00,$7c,$56,$ff,0
f_flob1_right_splat
    dta $00,$00,$00,$00,$3e,$6a,$ff,0
f_flob1_left_move
    dta $00,$00,$70,$fc,$fe,$ae,$7e,0
f_flob1_right_move
    dta $00,$00,$0e,$3f,$7f,$75,$7e,0
f_flob1_poof1
    dta $00,$00,$3c,$7e,$5a,$3c,$18,0
f_flob1_poof2
    dta $00,$3c,$5a,$bd,$66,$42,$24,0
