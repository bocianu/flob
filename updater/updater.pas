program flob_updater;
{$librarypath '../../blibs/'}
uses atari, b_system, b_maxFlash8Mb, sysutils, crt;

const
{$i const.inc}
{$i '../const.inc'}

{$r resources.rc}
{$i '../types.inc'}
{ $i interrupts.inc}

label pgmend;

var
    sector:byte;
    bank:byte;
    fileNum:byte;
    sectorFiles:array [0..7] of byte;
    sectorFilesCount:byte;
    filename:TString;
    buffer: array[0..8191] of byte absolute BANK_BUFFER;
    banks: array [0..128] of byte absolute $D500;   
    version_a, version_b, version_c: byte;
    GINTLK: byte absolute $03FA;  // @nodoc 
    TRIG3: byte absolute $D013;   // @nodoc 
    tryToFix, genuine: boolean;

procedure SetBank0;
begin
    wsync := 0;
    banks[0]:=0;
    GINTLK := TRIG3; 
end;


{$i '../saving_routines.inc'}

function IsFlobCartIn:boolean;
begin
    result:=true;
    wsync:=0;
    SetBank(0);
    GINTLK := TRIG3; 
    if Peek(VER_MAIN_ADDR) > 1 then result:=false;
    if Peek(VER_MAJOR_ADDR) > 9 then result:=false;
    if Peek(VER_MINOR_ADDR) > 9 then result:=false;
    if Peek(HEADER_0) <> 0 then result:=false;
    if Peek(HEADER_4) <> 4 then result:=false;
end;

function GetCartVersion:TString;
begin
    result:=' .  ';
    SetBank0;
    version_a := Peek(VER_MAIN_ADDR);
    version_b := Peek(VER_MAJOR_ADDR);
    version_c := Peek(VER_MINOR_ADDR);
    result[1] := char(version_a + 48);
    result[3] := char(version_b + 48);
    result[4] := char(version_c + 48);
end;

procedure BuildFilename(b:byte);
begin
    Str(b,filename);
    filename := Concat('D:BANK_',filename);
    filename := Concat(filename,'.BNK');
end;


procedure GetSectorFiles(sec:byte);
var basenum:byte;
    bnk:byte;
begin
    basenum := sec shl 3;
    bnk := 0;
    sectorFilesCount:=0;
    repeat 
        sectorFiles[bnk] := 0;
        BuildFilename(basenum);
        //Writeln(filename);
        if FileExists(filename) then begin
            sectorFiles[bnk] := 1;
            Inc(sectorFilesCount)
        end; //else Writeln('file not found: ',filename);
        
        inc(basenum);
        inc(bnk);
    until bnk = 8;
end;

procedure ReadBankFile(num:byte;address:word);
var f:file;
    numread:word;
begin   
    BuildFilename(num);
    Assign(f, filename); 
    reset(f, 256);
    blockread(f, buffer, 32, NumRead);
    //writeln(ioresult, ',', numread);
    close(f);
end;

function GetDiskVersion:TString;
var a,b,c:byte;
begin
    result:=' .  ';
    ReadBankFile(0,BANK_BUFFER);
    version_a := Peek(BANK_BUFFER + $1FF7);
    version_b := Peek(BANK_BUFFER + $1FF8);
    version_c := Peek(BANK_BUFFER + $1FF9);
    result[1] := char(version_a + 48);
    result[3] := char(version_b + 48);
    result[4] := char(version_c + 48);
end;

function Verify(vbank:byte;src,dest,len:word):boolean;
begin
    wsync:=0;
    SetBank(vbank);
    GINTLK := TRIG3; 
    result := CompareMem(pointer(src),pointer(dest),len)
end;

function CurrentVersionBelow(a,b,c:byte): boolean;
begin
    result := false;
    if version_a < a then exit(true);    // 1   2
    if version_a > a then exit(false);   // 2   1
    // main equal 
    if version_b < b then exit(true);    // 0   1 
    if version_b > b then exit(false);   // 1   0 
    // major equal
    if version_c < c then exit(true);  // 1   2
end;

procedure FixMinesAchievementDetection;
begin
    Writeln('Fix: mines secret achievement');
    wsync:=0;
    asm sei end;
    if FindLastSave then begin
        Writeln('Save File found.');
        Writeln('Checking if fix needed:');
        wsync:=0;
        RestoreLastSave;
        GINTLK := TRIG3; 
        GetWorldStats(4); // Mines
        if worldStats.maxSecrets > 2 then begin
            worldStats.maxSecrets := 2;
            if worldStats.secretAllCount = 0 then worldStats.secretAllCount := 1;
            Writeln('Fix Applied. Saving New Values.');
            SetWorldStats(4);
            StoreStats;
            Writeln('Fixed.');
        end else begin
            Writeln('Fix not needed, skipping...');
        end;
    end else begin
        Writeln('No proper save file found.');
    end;
    wsync:=0;
    GINTLK := TRIG3; 
    asm cli end;
end;

begin
    tryToFix := false;
    FillByte(pointer(VIDEO_RAM_UPDATER_ADDRESS),40*25,0);
    Pause;
    savmsc := VIDEO_RAM_UPDATER_ADDRESS;  // set custom video address
    SDLSTL := DISPLAY_LIST_UPDATER_ADDRESS;
    CursorOff;
    GotoXY(3,1);
    Writeln('Flob cartridge updater'*);
    Writeln;
    Write('New version to be uploaded: ');
    Writeln(GetDiskVersion);
    genuine := IsFlobCartIn;
    if not genuine then begin
        Writeln;
        Writeln('This is (probably) not a genuine');
        Writeln('FloB cartridge.');
        Writeln;
        Writeln('Do you want to continue? (y/n)');
        Writeln;
        if readkey <> 'y' then goto pgmend;
    end else begin
        Write('Version detected on cart:   ');
        Writeln(GetCartVersion);
        tryToFix := true;
    end;
    Writeln('Do you want to continue? (y/n)');
    Writeln;
    if readkey <> 'y' then goto pgmend;

    if tryToFix then begin
        if CurrentVersionBelow(1,0,2) then begin
            FixMinesAchievementDetection;
        end;
    end;

    Writeln('Upgrade started:');
    Writeln('Do not turn your ATARI off now!'*);
    Writeln;
    
    sector := 0;
    
    repeat
        Write('Sector: ');
        Write(sector);
        GetSectorFiles(sector);
        Write(' - Banks to update: ');
        Writeln(sectorFilesCount);
        
        if sectorFilesCount>0 then begin
            Write('Erasing Sector ',sector);
            EraseSector(sector);
            Writeln(' - Done.');
            bank:=0;
            repeat
                if sectorFiles[bank]<>0 then begin
                    fileNum := (sector shl 3) + bank;
                    Write('BANK:',fileNum,' ');
                    Write('READ'*' ');
                    ReadBankFile(fileNum,BANK_BUFFER);
                    Write('WRITE'*' ');
                    asm sei end;
                        BurnPages(sector,Hi(BANK_BUFFER),bank shl 5,32);
                    //BurnBlock(fileNum,BANK_BUFFER,$A000,$2000);
                    asm cli end;
                    Write('VERIFY'*' ');
                    if not Verify(filenum,$a000,BANK_BUFFER,$2000) then 
                        Writeln('KO!!!')
                    else Writeln('OK');
                end;
                inc(bank);
            until bank = 8;
        
        end;
        inc(sector);
    until sector = 7;
    Writeln;
    Pause;
    SetBank0; 
     
    Writeln('Operation finished! Press '+'RESET'*);
    ReadKey;
    
pgmend:

    TextMode(0);
    SystemReset;
end.
