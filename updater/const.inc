//
// ;*** define your project wide constants here
// 
// ;*** I like to keep memory locations at top of this file
//

VIDEO_RAM_UPDATER_ADDRESS = $5000;
DISPLAY_LIST_UPDATER_ADDRESS = $5F00;
BANK_BUFFER = $6000;

VER_MAIN_ADDR = $BFF7;
VER_MAJOR_ADDR = $BFF8;
VER_MINOR_ADDR = $BFF9;
HEADER_0 = $BFFC;
HEADER_4 = $BFFD;


