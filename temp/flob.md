---
Title: FloB
Description: FloB, 8bit atari action-adventure-platformer game by bocianu
Author: bocianu
Date: 2021-07-10
Robots: index, follow
Template: game
Tags: game
Image: flob0.png,flob1.png,flob2.png,flob3.png,flob4.png,flob6.png,flob5.png
Download: flob.1.0.1.car
DownloadDir: games
Short: Flob is a platform-action-adventure game written in Mad-Pascal, for 8-bit Atari computers.
Lang: en
---
###Source Code: https://gitlab.com/bocianu/flob
###Music Director: LiSU
###Music Artists: LiSU, AceMan, Miker, PG, Zoltar X
###Translations/proofreading: Fei
###Testers: zbyti, RetroBorsuk, Mq, Dely, koala, kris3d 
---
Flob is a platform-action-adventure game written in Mad-Pascal, for 8-bit Atari computer.
In this game you need to travel trought hundreds of locations to find hidden slime ingredients
and formula, hidden by your creator Dr Torff. Using slime you can manipulate the gravity and
turn the world upside down. Flob can not jump so it can be the only way to get to higher places.
When you will get the formula and ingredients, you can become immortal beeing and rule the world.

You can download game manual [here](../assets/flob/flob_manual.pdf)

##How to buy a physical copy of the game?
Write an email to [mq666xx@gmail.com](mailto://mq666xx@gmail.com) and ask for game availability.

##How to contribute the author?
This game is free to download, but if you like it, you can contribute the author. Please use "donate" button 
in the main menu and use paypal to contribute, or just contact me on my [email](mailto://bocianu@gmail.com) if you prefer any other way.

##Game Requirements:
- stock Atari XL/XE with 64kB RAM
- stereo Pokey (optional)

##Main game features:
- 6 large maps consisting over 140 hand drawn locations,
- each map has unique style, mood and music,
- story driven gameplay with intro and outro, 
- each location has secret rooms with big slime rewards,
- achievements system with 82 trophies to collect,
- you can unlock in-game statistics, to track your progress,
- your progress is stored on cartridge,
- built in "cheat mode" 

##Controls during the game:
Joystick or arrow keys: move hero left or right  
Fire or shift key: flip the world upside down  
Esc: hold to start again, hold a second time to quit the current game  
OPTION: disable music  
SELECT: switch to the next tune  

##Entering "cheat-mode":
Cheat mode allows you to visit all game maps without unlocking them. It also gives you huge amount of slime on game
start to make your life easier. But in the this mode your game progress and achievements will not be saved on cartridge.
To turn on the cheat mode, keep HELP key pressed during the game boot procedure (after Mq logo). When screen will turn
pink - release the HELP button.

##Updating the game:
- Download .atr image with latest version of FloB and mount it in any capable device connected to your Atari
- Put your cartridge in the cart slot
- Turn on your Atari with OPTION key pressed - it will pass cart booting and switch to normal OS booting routine
- Flob updater should be booted from the disk image
- Follow the on screen intructions

###ATR update images:
- [FloB 1.0.1](../assets/flob/flob.1.01.atr) - ([view changelog](https://gitlab.com/bocianu/flob/-/blob/master/CHANGELOG#L1))
- [FloB 1.0.0](../assets/flob/flob.1.0.0.atr) - (initial release)

##Reseting game to the factory defaults:
You can reset all the game progress, and bring back the factory setttings by keeping START-SELECT-OPTION pressed 
during the startup procedures. When the screen turns white, quickly press HELP button. Your progress will be cleared.
BE CAREFUL - IT CANNOT BE UNDONE.

##AVG cart image:
There is special image for AVG cart users, that automaticaly stores your progress on the device:
- [FloB 1.0.1 AVG-cart](../assets/flob/flob-1.0.1_avgcart_only.zip)
